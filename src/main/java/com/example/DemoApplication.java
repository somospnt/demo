package com.example;

import com.example.repository.FeriadoRepository;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.servlet.CamelHttpTransportServlet;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class DemoApplication extends SpringBootServletInitializer {

    
    @Autowired
    private FeriadoRepository feriadoRepository;

    @Bean
    ServletRegistrationBean servletRegistrationBean() {
        ServletRegistrationBean servlet = new ServletRegistrationBean(
                new CamelHttpTransportServlet(), "/camel/*");
        servlet.setName("CamelServlet");
        return servlet;
    }

    @Component
    class FeriadosApi extends RouteBuilder {

        @Override
        public void configure() {
            restConfiguration()
                    .component("servlet")
                    .contextPath("/camel")
                    .bindingMode(RestBindingMode.json);
            rest("/feriados").get().produces("application/json;charset=utf-8").route().bean(feriadoRepository, "findAll()").endRest();
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
